package ru.t1.karimov.tm.api.model;

import ru.t1.karimov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
