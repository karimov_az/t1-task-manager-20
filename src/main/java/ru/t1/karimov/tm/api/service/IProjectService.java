package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project, IProjectRepository> {

    Project create(String userId, String name) throws AbstractFieldException;

    Project create(String userId, String name, String description) throws AbstractFieldException;

    Project updateById(String userId, String id, String name, String description) throws AbstractException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Project changeProjectStatusById (String userId, String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex (String userId, Integer index, Status status) throws AbstractException;

}
