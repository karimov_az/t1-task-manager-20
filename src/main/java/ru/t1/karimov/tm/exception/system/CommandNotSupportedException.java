package ru.t1.karimov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command cannot be empty...");
    }

    public CommandNotSupportedException(String command) {
        super("Error! Command \"" + command + "\" not supported...");
    }

}
