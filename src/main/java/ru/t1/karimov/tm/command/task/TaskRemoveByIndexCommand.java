package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        getTaskService().removeByIndex(userId, index);
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

}
