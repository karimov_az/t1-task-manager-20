package ru.t1.karimov.tm.command.system;

import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
