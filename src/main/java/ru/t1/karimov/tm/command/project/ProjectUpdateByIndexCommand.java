package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(userId, index, name, description);
    }

    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public String getDescription() {
        return "Update project by index.";
    }

}
