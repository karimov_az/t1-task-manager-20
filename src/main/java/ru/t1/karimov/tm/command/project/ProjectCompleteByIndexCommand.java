package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

}
