package ru.t1.karimov.tm.command.user;

import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public String getDescription() {
        return "Update profile of current user.";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
