package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[TASKS CLEAR]");
        getTaskService().clear(userId);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}
