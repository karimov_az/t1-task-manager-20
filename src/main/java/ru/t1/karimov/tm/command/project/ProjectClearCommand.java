package ru.t1.karimov.tm.command.project;

import ru.t1.karimov.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[PROJECT CLEAR]");
        getProjectService().clear(userId);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

}
