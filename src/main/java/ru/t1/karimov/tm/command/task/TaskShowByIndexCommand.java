package ru.t1.karimov.tm.command.task;

import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[TASK LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show task by index.";
    }

}
