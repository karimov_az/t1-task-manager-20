package ru.t1.karimov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alfred Karimov");
        System.out.println("e-mail: alfred@karimov.ru");
        System.out.println("e-mail: alfred_test@karimov.ru");
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

}
